#! /bin/bash


# Workon - easily move throught your projects.
# Author Felipe Freire
# 
#

# 20220706 1st version v0.1
# 20220725 version v0.2
# 20221125 version V0.3


### USE ##################################################################
# 
# 1. create a hidden directory in your home dir. Defaults to ".projects"
# 2. source workon.sh
# 3. have fun.
#
# * if you like workon, you can source it from .bashrc.
#
### END ##################################################################


# setting variables.
export projects_folder=$HOME/.projects

# checking if $projects_folder variable is not empty.
[ "$projects_folder" ] || {
	echo "workon: there is no projects folder."
}

version="v0.3"

workon_menu="	-- Workon --

		Usage

	workon --index [number]

	workon --name [name_of_directory] 

"


workon() {

	# checking for at least one argument.
	if [ "$#" -gt 0 ]; then
		case $1 in
			# --index option to move by project index.
			-i | --index )
			# for loop that store projects into a group, later used
			# for searching projects by index.
				local group=()
				for n in $(ls "$projects_folder"); do
					group+=("$n")
				done
				# then move into it.
				cd "$projects_folder/${group["$2"]}"
				;;
			# --name option to move by project name.
			-n | --name )
			# if project with that name exists, move into it
			# or raise me an error.
				if [ -d "$projects_folder/$2" ]; then
					cd "$projects_folder/$2"
				else
					echo "workon: no project named $2"
				fi
				;;
			-h | --help )
				echo "$workon_menu"
				;;
			-v | --version )
				echo "$version"
				;;
		esac
	fi
}

