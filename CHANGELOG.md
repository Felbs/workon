Changelog for workon


All notable changes to this project will be documented in this file.

version 0.1 released in 2022 07 06

Version 0.2 released in 2022 07 25

. added python venv support

Version 0.3 released in 2022 11 25

. workon now have two search options:

	--name for searching project by name. (-n)

	--index for searching projects by index. (-i)

	. removed python venv support

